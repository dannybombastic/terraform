<?php

namespace App\Form;

use App\Entity\Company;
use App\Entity\Experience;
use DateTime;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExperienceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('expcompany', EntityType::class, [
                'class' => Company::class,
                'choice_label' => function (Company $company) {
                    return $company->getName();
                },
                'label' => 'Company',
                'attr' => [
                    'class' => 'form-control',
                ],
                'choice_attr' => function ($choice, $key, $value) {
                    // adds a class like attending_yes, attending_no, etc
                    return ['class' => 'attending_' . strtolower($key)];
                },
            ])
            ->add('expname', TextType::class, [
                'label' => 'Job title'
            ])
            ->add('expdescription', TextareaType::class, [
                'label' => 'Job Experience',
                'attr' => ['rows' => 6]
            ])->add('expfrom', DateType::class, [
                'label' => 'Starts On',
                'data' => new DateTime()

            ])
            ->add('expto', DateType::class, [
                'label' => 'Finish On',
                'data' => new DateTime()
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Experience::class,

        ]);
    }
}
