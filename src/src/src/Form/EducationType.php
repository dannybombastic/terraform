<?php

namespace App\Form;

use App\Entity\Education;
use DateTime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
 

class EducationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('school', TextType::class,[
                'label'=> "School name"
            ])
            ->add('degree', TextType::class, [
                'label'=> "Degree name"
            ])
            ->add('degreedate', DateType::class, [
                'data' => new DateTime(),
                'label'=> 'Degree date'
                
            ])
          
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Education::class,
        ]);
    }
}
