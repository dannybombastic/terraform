<?php

namespace App\DataFixtures;

use App\Entity\ApiToken;
use App\Entity\User;
use App\DataFixtures\BaseFixture;
use App\Entity\GitRepositories;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends BaseFixture
{
    
    public function loadData(ObjectManager $manager)
    {
        /**
         * we start creating the integration of 
         * git repos from user
         */
        $repos = new GitRepositories();
        $repos->setGitlabaccesstoken("RREcW9pQLT8SVJqqoLEy");
        $repos->setGitladid("4056816");
        $repos->setUsernamegitlab("dannybombastic");
        $repos->setGitlabispub(true);
        $repos->setGithubispub(false);
        $repos->setGitlaburl("https://gitlab.com");
        
        /**
         * we create a default user with role admind
         */
        $admin = new User();
        $admin->setName('Daniel Urbano');
        $admin->setEmail('dannybombastic@gmail.com');
        $admin->setInfojobProfile($this->faker->url);
        $admin->setLinkedidProfile($this->faker->url);
        $admin->setAgreeterms(true);
        $admin->addGit($repos);
        $role = $admin->getRoles();

        if (!in_array('ROLE_ADMIN', $role, true)) {
            array_push($role, 'ROLE_ADMIN');
            $admin->setRoles($role);
        }
        $admin->setPassword($this->encoder->encodePassword(
            $admin,
            '659011563'
        ));

        $token = new ApiToken($admin);

        $manager->persist($token);
        $manager->flush();

        $repos = $admin->getGit()[0]->setGitlabjsn($this->gitLabService->saveGitlabInfo($admin));
        $manager->persist($repos);    
        $manager->flush();
        

        $this->createMany(User::class, 20, function (User $users, $count) {
            $users->setName($this->faker->name);
            $users->setEmail($this->faker->email);
            $users->setInfojobProfile($this->faker->url);
            $users->setLinkedidProfile($this->faker->url);
            $users->setAgreeterms(true);
            $users->setPassword($this->encoder->encodePassword(
                $users,
                '659011563'
            ));
        });

        $manager->flush();
    }
}
