<?php

namespace App\DataFixtures;



use App\DataFixtures\BaseFixture;
use App\Entity\Education;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class EducationFixture extends BaseFixture implements DependentFixtureInterface
{
    public function loadData(ObjectManager $manager)
    {

        $this->createMany(Education::class, 75, function (Education $education, $count) {
            // create 20 products! Bam!
            $education->setSchool($this->faker->userName);
            $education->setDegree($this->faker->realText($maxNbChars = 200, $indexSize = 2));
            $education->setDegreedate($this->faker->dateTimeBetween('-100 days', '-1 days'));
            $education->setUserid($this->getReference(User::class . '_' . $this->faker->numberBetween(0, 19)));
        });
        $manager->flush();
    }


    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
