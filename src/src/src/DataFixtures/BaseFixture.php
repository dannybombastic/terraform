<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\ApiToken;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker\Factory;
use App\Service\GitLabService;
use Faker\Provider\Lorem;

abstract class BaseFixture extends Fixture
{
    /** @var ObjectManager */
    private $manager;

    /** @var Lorem */
    protected $faker;
    /** @var UserPasswordEncoderInterface */
    protected $encoder = [];

    protected GitLabService $gitLabService;

    private $referenceIndex;

    public function __construct(UserPasswordEncoderInterface $encoder, GitLabService $gitLabService)
    {
        $this->encoder = $encoder;
        $this->gitLabService = $gitLabService;
    }

    abstract protected function loadData(ObjectManager $em);

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->faker = Factory::create();
        $this->loadData($manager);
    }

    protected function createMany(string $className, int $count, callable $factory)
    {
        for ($i = 0; $i < $count; ++$i) {
            if (ApiToken::class === $className) {
                $entity = new $className($this->getReference(User::class.'_'.$this->faker->numberBetween(0, 19)));
            } else {
                $entity = new $className();
            }

            $factory($entity, $i);

            $this->manager->persist($entity);
            // store for usage later as App\Entity\ClassName_#COUNT#
            $this->addReference($className.'_'.$i, $entity);
        }
    }

    protected function getRandomReference(string $className)
    {
        if (!isset($this->referencesIndex[$className])) {
            $this->referencesIndex[$className] = [];

            foreach ($this->referenceRepository->getReferences() as $key => $ref) {
                if (0 === strpos($key, $className.'_')) {
                    $this->referencesIndex[$className][] = $key;
                }
            }
        }

        if (empty($this->referencesIndex[$className])) {
            throw new \Exception(sprintf('Cannot find any references for class "%s"', $className));
        }

        $randomReferenceKey = $this->faker->randomElement($this->referencesIndex[$className]);

        return $this->getReference($randomReferenceKey);
    }

    protected function getRandomReferences(string $className, int $count)
    {
        $references = [];
        while (count($references) < $count) {
            $references[] = $this->getRandomReference($className);
        }

        return $references;
    }


}
