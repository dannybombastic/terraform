<?php

namespace App\DataFixtures;



use App\DataFixtures\BaseFixture;
use App\Entity\Resume;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ResumeFixture extends BaseFixture implements DependentFixtureInterface
{
    public function loadData(ObjectManager $manager)
    {

        $this->createMany(Resume::class, 20, function (Resume $resume, $count) {
            // create 20 products! Bam!
            $resume->setName($this->faker->userName);
            $resume->setcontent($this->faker->realText($maxNbChars = 200, $indexSize = 2));
            $resume->setUserid($this->getReference(User::class . '_' . $this->faker->numberBetween(0, 19)));
        });
        $manager->flush();
    }


    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
