<?php

namespace App\Repository;

use App\Entity\Experience;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Experince|null find($id, $lockMode = null, $lockVersion = null)
 * @method Experince|null findOneBy(array $criteria, array $orderBy = null)
 * @method Experince[]    findAll()
 * @method Experince[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExperienceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Experience::class);
    }

    // /**
    //  * @return Experince[] Returns an array of Experince objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function getWithShareQuerybuilder(?string $term, string $user_id): QueryBuilder
    {

        $qb = $this->createQueryBuilder('r')
            ->andWhere("r.userid = $user_id");

        if ($term) {
            $qb->andWhere('r.userid = :user AND r.expname LIKE :term OR r.expdescription LIKE :term')
                ->setParameter('term', '%' . $term . '%')
                ->setParameter('user', $user_id);
        }

        return $qb
            ->orderBy('r.createat', 'DESC');
    }

    public function getCompanyByUser($term, string $user_id): QueryBuilder
    {

        $qb = $this->createQueryBuilder('r')
        
            ->innerJoin('r.expcompany', 'a')
            ->andWhere("r.userid = $user_id")
            ->addSelect('a')
            ;
            
            if ($term) {
                $qb->andWhere('r.userid = :user AND a.comdescription LIKE :term OR a.name LIKE :term')
                    ->setParameter('term', '%' . $term . '%')
                    ->setParameter('user', $user_id);
            }

        return $qb
            ->orderBy('r.createat', 'DESC');
    }

    /*
    public function findOneBySomeField($value): ?Experince
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
