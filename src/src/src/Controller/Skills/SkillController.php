<?php

namespace App\Controller\Skills;

use App\Entity\Skill;
use App\Form\SkillType;
use App\Repository\SkillRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/skill")
 * @IsGranted("ROLE_USER")
 */
class SkillController extends AbstractController
{
    /**
     * @Route("/", name="app_skill_index", methods={"GET"})
     */
    public function index(SkillRepository $skillRepository): Response
    {
        $skills = $skillRepository->findBy(['userid'=> $this->getUser()->getId()]);

        return $this->render('skill/index.html.twig', [
            'skills' => $skills,
        ]);
    }

    /**
     * @Route("/new", name="app_skill_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $skill = new Skill();
        $skill->setUserid($this->getUser());
        $form = $this->createForm(SkillType::class, $skill);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($skill);
            $entityManager->flush();

            return $this->redirectToRoute('app_skill_index');
        }

        return $this->render('skill/new.html.twig', [
            'skill' => $skill,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_skill_show", methods={"GET"})
     * @IsGranted("OWNER", subject="skill")
     */
    public function show(Skill $skill): Response
    {
        
        return $this->render('skill/show.html.twig', [
            'skill' => $skill,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_skill_edit", methods={"GET","POST"})
     * @IsGranted("OWNER", subject="skill")
     */
    public function edit(Request $request, Skill $skill): Response
    {
        $form = $this->createForm(SkillType::class, $skill);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('app_skill_index');
        }

        return $this->render('skill/edit.html.twig', [
            'skill' => $skill,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_skill_delete", methods={"DELETE"})
     * @IsGranted("OWNER", subject="skill")
     */
    public function delete(Request $request, Skill $skill): Response
    {
        if ($this->isCsrfTokenValid('delete'.$skill->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($skill);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_skill_index');
    }
}
