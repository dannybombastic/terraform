<?php

namespace App\Controller\Profiles;

use App\Entity\CvRoute;
use App\Entity\User;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/profiles")
 * 
 */
class ProfilesController extends AbstractController
{
    /**
     * @Route("/user/cv/{email}", name="app_profiles", methods={"GET"})
     */
    public function index(Request $request, UserRepository $userRepository): Response
    {

        $email = str_replace('/profiles/user/cv/', '', $request->getRequestUri());
        $msg = $email;

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
         
            $user = $userRepository->findOneByEmail($email);
            dump($user);
            if ($user) {
                $msg = null;
                $email = null;
                return $this->render('profiles/index.html.twig', [
                    'controller_name' => $user->getEmail(),
                    'user_cv' => $user,
                    'email' => $email,
                    'msg' => $msg
                ]);
            }
        }

        $msg = "Invalid email format or user doesn't exist";
        return $this->render('profiles/index.html.twig', [
            'controller_name' => 'profiles',
            'user_cv' => null,
            'email' => $email,
            'msg' => $msg
          
        ]);
    }


    /**
     * @Route("/user/profile/{user}", name="app_profiles_byid")
     */
    public function profile(User $user): Response
    {
        $user = $user;

        if ($user) {
            return $this->render('profiles/index.html.twig', [
                'controller_name' => $user->getEmail(),
                'user_cv' => $user,
                'email' => null,
                'msg' => null,
            ]);
        }
    }
    /**
     * @Route("/user/profile", name="app_profiles_dani")
     */
    public function daniel(): Response
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findBy(['email' => 'dannybombastic@gmail.com'])[0];

        return $this->render('profiles/daniel/index.html.twig', [
            'controller_name' => 'Daniel',
            'user_info' => $user
        ]);
    }


    /**
     * @Route("/list/profiles", name="app_profiles_list", methods={"GET"})
     */
    public function list(UserRepository $userRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $q = $request->query->get('q');

        $queryBuilder = $userRepository->getWithShareQuerybuilder($q);
        
        $paginator = $paginator->paginate(
            $queryBuilder,
            $request->query->getInt('page', 1),
            15
        );

        return $this->render('profiles/list/list.html.twig', [
            'pagination' => $paginator
        ]);
    }

    /**
     * @Route("/cv/ismael", name="app_profiles_isma")
     */
    public function ismael(): Response
    {
        return $this->render('profiles/ismael/index.html.twig', [
            'controller_name' => 'Ismael',
        ]);
    }

    /**
     * @Route("/cv/camacho", name="app_profiles_camacho")
     */
    public function camacho(): Response
    {
        return $this->render('profiles/camacho/index.html.twig', [
            'controller_name' => 'camacho',
        ]);
    }
}
