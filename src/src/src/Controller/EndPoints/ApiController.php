<?php

namespace App\Controller\EndPoints;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/v1", name="api")
 */

class ApiController extends AbstractController
{
    /**
     * @Route("/", methods={"POST"}, name="app_api")
     */
    public function index(Request $request): Response
    {
        $data = $request->request->get("datos");     
        
        return $this->json([
            "values" =>  $data
        ], 200,[],[]);
    }
}
