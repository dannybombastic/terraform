<?php

namespace App\Controller\Resume;

use App\Entity\Resume;
use App\Form\ResumeType;
use App\Repository\ResumeRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/resume")
 * @IsGranted("ROLE_USER")
 */
class ResumeController extends AbstractController
{
    /**
     * @Route("/", name="app_resume_index", methods={"GET"})
     */
    public function index(ResumeRepository $resumeRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $q = $request->query->get('q');
        $user_id = $this->getUser()->getId();

        $queryBuilder = $resumeRepository->getWithShareQuerybuilder($q, $user_id);
         
        $paginator = $paginator->paginate(
            $queryBuilder,
            $request->query->getInt('page', 1)
        );
        
     
        return $this->render('resume/index.html.twig', [
            'pagination' => $paginator,
        ]);
    }

    /**
     * @Route("/new", name="app_resume_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $resume = new Resume();
        
 
        $form = $this->createForm(ResumeType::class, $resume);
        $form->handleRequest($request);

        $resume->setUserid($this->getUser());

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($resume);
            $entityManager->flush();
            $this->addFlash('success', 'Aticle created knowledge is power!');

            return $this->redirectToRoute('app_resume_index');
        }

        return $this->render('resume/new.html.twig', [
            'resume' => $resume,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_resume_show", methods={"GET"})
     * @IsGranted("OWNER", subject="resume")
     */
    public function show(Resume $resume): Response
    {
        return $this->render('resume/show.html.twig', [
            'resume' => $resume,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_resume_edit", methods={"GET","POST"})
     * @isGranted("OWNER", subject="resume")
     */
    public function edit(Request $request, Resume $resume): Response
    {
        $form = $this->createForm(ResumeType::class, $resume);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('app_resume_index');
        }

        return $this->render('resume/edit.html.twig', [
            'resume' => $resume,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_resume_delete", methods={"DELETE"})
     * @isGranted("OWNER", subject="resume")
     */
    public function delete(Request $request, Resume $resume): Response
    {
        if ($this->isCsrfTokenValid('delete'.$resume->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($resume);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_resume_index');
    }
}
