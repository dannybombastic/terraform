<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;



class JomeController extends AbstractController
{
    /**
     * @Route("/", name="app_jome")
     */
    public function index(): Response
    {  
        return $this->render('jome/index.html.twig', [
            'controller_name' => 'JomeController',
            'user' => $this->getUser() ? $this->getUser()->getApiTokens()[0] : null
        ]);
    }

  
}
