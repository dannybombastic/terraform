<?php

namespace App\Controller;

use App\Entity\GitRepositories;
use App\Form\GitRepositoriesType;
use App\Repository\GitRepositoriesRepository;
use App\Service\GitLabService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/git/repositories")
 * @IsGranted("ROLE_USER")
 */
class GitRepositoriesController extends AbstractController
{
    /**
     * @Route("/", name="app_git_repositories_index", methods={"GET"})
     */
    public function index(GitRepositoriesRepository $gitRepositoriesRepository): Response
    {


        $repo = $gitRepositoriesRepository->findOneBy(["gitowner" => $this->getUser()->getId()]);
        if($repo){
            return $this->render('git_repositories/index.html.twig', [
                'git_repositories' => $repo,
            ]);
        }
        return $this->render('git_repositories/index.html.twig', [
            'git_repositories' => null,
        ]);
       
    }

    /**
     * @Route("/new", name="app_git_repositories_new", methods={"GET","POST"})
     */
    public function new(Request $request, GitLabService $gitService, GitRepositoriesRepository $gitRepositoriesRepository): Response
    {

    
        $gitRepositorie = new GitRepositories();
        $gitRepositorie->setGitowner($this->getUser());
        $form = $this->createForm(GitRepositoriesType::class, $gitRepositorie);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($gitRepositorie);
            $entityManager->flush();

            if ($gitRepositorie->getGitlabispub() && $gitRepositorie->getGitlabaccesstoken()) {
                $data_json = $gitService->saveGitlabInfo($this->getUser());
                $gitRepositorie->setGitlabjsn($data_json);
                $entityManager->persist($gitRepositorie);
                $entityManager->flush();
            }

            return $this->redirectToRoute('app_git_repositories_index');
        }

        return $this->render('git_repositories/new.html.twig', [
            'git_repository' => $gitRepositorie,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_git_repositories_show", methods={"GET"})
     * @IsGranted("OWNER", subject="gitRepositorie")
     */
    public function show(GitRepositories $gitRepositorie): Response
    {
        return $this->render('git_repositories/show.html.twig', [
            'git_repository' => $gitRepositorie,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_git_repositories_edit", methods={"GET","POST"})
     * @IsGranted("OWNER", subject="gitRepository")
     */
    public function edit(Request $request, GitRepositories $gitRepository): Response
    {
        $form = $this->createForm(GitRepositoriesType::class, $gitRepository);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('app_git_repositories_index');
        }

        return $this->render('git_repositories/edit.html.twig', [
            'git_repository' => $gitRepository,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_git_repositories_delete", methods={"DELETE"})
     * @IsGranted("OWNER", subject="gitRepository")
     */
    public function delete(Request $request, GitRepositories $gitRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $gitRepository->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($gitRepository);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_git_repositories_index');
    }
}
