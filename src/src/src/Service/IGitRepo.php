<?php

namespace App\Service;

use App\Entity\User;

interface IGitRepo
{
    public function getRepo(User $user): array;
}
