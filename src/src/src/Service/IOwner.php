<?php

namespace App\Service;

use App\Entity\User;

interface IOwner
{
    public function isOwner(User $user): ?bool;
}
