<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\GitRepositoriesRepository;

class GitHubService
{

    const GITLAB_URL = 'https://gitlab.com/api/v4';
    const GITLHUB_URL = 'https://api.github.com';

 
    const GITHUB_USER_PARAMS = [
        "user" => "/user/repos"
    ];
    const GITHUB_HEADER = [
        "Authorization" => 'Authorization: token <token>',
        "Accept" => "application/vnd.github.v3+json"
    ];

    public GitRepositoriesRepository $gitRepository;

    public function __construct(GitRepositoriesRepository $gitRepository)
    {
        $this->gitRepository = $gitRepository;
    }

    public function saveGitlabInfo(User $userId): array
    {

        return $this->curlGitHub($userId);
    }

    public function curlGitHub(User $userId) : array
    {
        $gitlab = $this->gitRepository->findOneBy(["gitowner" => $userId->getId()]);
        $token = $gitlab->getGitlabaccesstoken();

        $url = self::GITLHUB_URL
            . self::GITHUB_USER_PARAMS['user']
            . $userId
            . self::GITHUB_USER_PARAMS['token']
            . $token;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $data = curl_exec($ch);
        dd($data);
        curl_close($ch);

        return json_decode($data, true);
    }
}
