<?php

namespace App\Entity;

use App\Repository\ResumeRepository;
use App\Service\IOwner;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=ResumeRepository::class)
 */
class Resume implements IOwner
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Please Enter atleast one name")
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Please Enter some content")
     */
    private $content;

    /**
     * @ORM\Column(type="date")
     */
    private $createat;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="resume")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userid;



    public function __construct()
    {
        $this->createat = new DateTime();
    }

    public function setUserid(?User $userid): self
    {
        $this->userid = $userid;

        return $this;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreateat(): ?\DateTimeInterface
    {
        return $this->createat;
    }

    public function setCreateat(\DateTimeInterface $createat): self
    {
        $this->createat = $createat;

        return $this;
    }

    public function getUserip(): ?User
    {
        return $this->userip;
    }

    public function setUserip(?User $userip): self
    {
        $this->userip = $userip;

        return $this;
    }

    public function isOwner(User $user): ?bool
    {
          return $this->userid === $user;
    }

    /**
     * Get the value of userid
     */ 
    public function getUserid()
    {
        return $this->userid;
    }
}
