<?php

namespace App\Entity;

use App\Repository\ExperienceRepository;
use App\Service\IOwner;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ExperienceRepository::class)
 */
class Experience implements IOwner
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $expname;
    
    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Please Enter atleast one name")
     */
    private $expdescription;

    /**
     * @ORM\Column(type="date")
     */
    private $expfrom;

    /**
     * @ORM\Column(type="date")
     */
    private $expto;

    /**
     * @ORM\Column(type="date")
     */
    private $createat;


    /**
     * @ORM\ManyToOne(targetEntity=Company::class,  cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $expcompany;

    /**
     * @ORM\ManyToOne(targetEntity=User::class,  inversedBy="experience")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userid;

    public function __construct()
    {
        $this->createat = new DateTime();
        $this->expfrom = new DateTime();
        $this->expto = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExpname(): ?string
    {
        return $this->expname;
    }

    public function setExpname(string $expname): self
    {
        $this->expname = $expname;

        return $this;
    }

    public function getExpdescription(): ?string
    {
        return $this->expdescription;
    }

    public function setExpdescription(string $expdescription): self
    {
        $this->expdescription = $expdescription;

        return $this;
    }

    public function getExpfrom(): ?\DateTimeInterface
    {
        return $this->expfrom;
    }

    public function setExpfrom(\DateTimeInterface $expfrom): self
    {
        $this->expfrom = $expfrom;

        return $this;
    }

    public function getExpto(): ?\DateTimeInterface
    {
        return $this->expto;
    }

    public function setExpto(\DateTimeInterface $expto): self
    {
        $this->expto = $expto;

        return $this;
    }

    public function getCreateat(): ?\DateTimeInterface
    {
        return $this->createat;
    }

    public function setCreateat(\DateTimeInterface $createat): self
    {
        $this->createat = $createat;

        return $this;
    }

    public function getExpCompany(): ?Company
    {
        return $this->expcompany;
    }

    public function setExpCompany(?Company $expcompany): self
    {
        $this->expcompany = $expcompany;

        return $this;
    }
 

    public function isOwner(User $user): ?bool
    {
          return $this->userid === $user;
    }

    /**
     * Set the value of userid
     *
     * @return  self
     */ 
    public function setUserid(?User $userid): self
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get the value of userid
     */ 
    public function getUserid()
    {
        return $this->userid;
    }
}
