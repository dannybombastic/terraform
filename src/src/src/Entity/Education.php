<?php

namespace App\Entity;

use App\Repository\EducationRepository;
use App\Service\IOwner;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EducationRepository::class)
 */
class Education implements IOwner
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $school;

    /**
     * @ORM\Column(type="text")
     */
    private $degree;


    /**
     * @ORM\Column(type="date")
     */
    private $degreedate;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="education")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setUserid(?User $userid): self
    {
        $this->userid = $userid;

        return $this;
    }

    public function getSchool(): ?string
    {
        return $this->school;
    }

    public function setSchool(string $school): self
    {
        $this->school = $school;

        return $this;
    }

    public function getDegree(): ?string
    {
        return $this->degree;
    }

    public function setDegree(string $degree): self
    {
        $this->degree = $degree;

        return $this;
    }


    public function getDegreedate(): ?\DateTimeInterface
    {
        return $this->degreedate;
    }

    public function setDegreedate(\DateTimeInterface $degreedate): self
    {
        $this->degreedate = $degreedate;

        return $this;
    }

    public function getUsereducation(): ?User
    {
        return $this->usereducation;
    }

    public function setUsereducation(?User $usereducation): self
    {
        $this->usereducation = $usereducation;

        return $this;
    }

    public function isOwner(User $user): ?bool
    {
          return $this->userid === $user;
    }
}
