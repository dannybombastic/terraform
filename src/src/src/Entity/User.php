<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 *  
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkedidProfile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $infojobProfile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phonenumber;

    /**
     * @ORM\OneToMany(targetEntity=Skill::class, mappedBy="userid", orphanRemoval=true)
     */
    private $skills;

    /**
     * @ORM\Column(type="date")
     */
    private $agreeterms;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $about;

    /**
     * @ORM\OneToMany(targetEntity=Education::class, mappedBy="userid", orphanRemoval=true)
     */
    private $education;

    /**
     * @ORM\OneToMany(targetEntity=Experience::class, mappedBy="userid", orphanRemoval=true)
     */
    private $experience;

    /**
     * @ORM\OneToMany(targetEntity=Resume::class, mappedBy="userid", orphanRemoval=true)
     */
    private $resume;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @ORM\OneToMany(targetEntity=ApiToken::class, mappedBy="userid", orphanRemoval=true)
     */
    private $apiTokens;

    /**
     * @ORM\OneToMany(targetEntity=GitRepositories::class, mappedBy="gitowner", cascade={"persist"})
     */
    private $git;

   

    public function __construct()
    {
        $this->skills = new ArrayCollection();
        $this->education = new ArrayCollection();
        $this->experience = new ArrayCollection();
        $this->resume = new ArrayCollection();
        $this->apiTokens = new ArrayCollection();
        $this->git = new ArrayCollection();
      
       
    }

    public function getAvatarUrl(int $size = null): string
    {
        $url = 'https://robohash.org/' . $this->getUserName();

        if ($size) {
            $url .= sprintf('?size=%dx%d', $size, $size);
        }

        return $url;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->name;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }


    public function getLinkedidProfile(): ?string
    {
        return $this->linkedidProfile;
    }

    public function setLinkedidProfile(?string $linkedidProfile): self
    {
        $this->linkedidProfile = $linkedidProfile;

        return $this;
    }

    public function getInfojobProfile(): ?string
    {
        return $this->infojobProfile;
    }

    public function setInfojobProfile(?string $infojobProfile): self
    {
        $this->infojobProfile = $infojobProfile;

        return $this;
    }

    public function getPhonenumber(): ?string
    {
        return $this->phonenumber;
    }

    public function setPhonenumber(?string $phonenumber): self
    {
        $this->phonenumber = $phonenumber;

        return $this;
    }

    /**
     * @return Collection|Skill[]
     */
    public function getSkills(): Collection
    {
        return $this->skills;
    }

    public function addSkill(Skill $skill): self
    {
        if (!$this->skills->contains($skill)) {
            $this->skills[] = $skill;
            $skill->setUserid($this);
        }

        return $this;
    }

    public function removeSkill(Skill $skill): self
    {
        if ($this->skills->removeElement($skill)) {
            // set the owning side to null (unless already changed)
            if ($skill->getUserid() === $this) {
                $skill->setUserid(null);
            }
        }

        return $this;
    }

    public function getAgreeterms(): bool
    {
        return null === $this->agreeterms;
    }

    public function setAgreeterms(bool $agree): self
    {
        $this->agreeterms = $agree ? new DateTime() : null;

        return $this;
    }


    public function user_agreeterms()
    {
        $this->setAgreeterms($this->getAgreeterms());
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAbout(): ?string
    {
        return $this->about;
    }

    public function setAbout(string $about): self
    {
        $this->about = $about;

        return $this;
    }

    /**
     * @return Collection|education[]
     */
    public function getEducation(): Collection
    {
        return $this->education;
    }

    public function addEducation(education $education): self
    {
        if (!$this->education->contains($education)) {
            $this->education[] = $education;
            $education->setUsereducation($this);
        }

        return $this;
    }

    public function removeEducation(education $education): self
    {
        if ($this->education->removeElement($education)) {
            // set the owning side to null (unless already changed)
            if ($education->getUsereducation() === $this) {
                $education->setUsereducation(null);
            }
        }

        return $this;
    }




    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return Collection|Experince[]
     */
    public function getExperience(): Collection
    {
        return $this->experience;
    }

    public function addExperience(Experience $experience): self
    {
        if (!$this->experience->contains($experience)) {
            $this->experience[] = $experience;
            $experience->setUserid($this);
        }

        return $this;
    }

    public function removeExperience(Experience $experience): self
    {
        if ($this->experience->removeElement($experience)) {
            // set the owning side to null (unless already changed)
            if ($experience->isOwner($this)) {
                $experience->setUserid(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Resume[]
     */
    public function getResume(): Collection
    {
        return $this->resume;
    }

    public function addResume(Resume $resume): self
    {
        if (!$this->resume->contains($resume)) {
            $this->resume[] = $resume;
            $resume->setUserip($this);
        }

        return $this;
    }

    public function removeResume(Resume $resume): self
    {
        if ($this->resume->removeElement($resume)) {
            // set the owning side to null (unless already changed)
            if ($resume->getUserip() === $this) {
                $resume->setUserip(null);
            }
        }

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * @return Collection|ApiToken[]
     */
    public function getApiTokens(): Collection
    {
        return $this->apiTokens;
    }

    /**
     * @return Collection|GitRepositories[]
     */
    public function getGit(): Collection
    {
        return $this->git;
    }

    public function addGit(GitRepositories $git): self
    {
        if (!$this->git->contains($git)) {
            $this->git[] = $git;
            $git->setGitowner($this);
        }

        return $this;
    }

    public function removeGit(GitRepositories $git): self
    {
        if ($this->git->removeElement($git)) {
            // set the owning side to null (unless already changed)
            if ($git->getGitowner() === $this) {
                $git->setGitowner(null);
            }
        }

        return $this;
    }

  
 
  
}
