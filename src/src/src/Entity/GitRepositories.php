<?php

namespace App\Entity;

use App\Repository\GitRepositoriesRepository;
use App\Service\IOwner;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GitRepositoriesRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class GitRepositories implements IOwner
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $githuburl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gitlaburl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gitlabaccesstoken;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $githubaccesstoken;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $usernamegitlab;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $usernamegithub;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $gitlabispub;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $githubispub;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $githubjsn = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $gitlabjsn = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gitladid;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="git")
     */
    private $gitowner;

    
 

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGithuburl(): ?string
    {
        return $this->githuburl;
    }

    public function setGithuburl(?string $githuburl): self
    {
        $this->githuburl = $githuburl;

        return $this;
    }

    public function getGitlaburl(): ?string
    {
        return $this->gitlaburl;
    }

    public function setGitlaburl(?string $gitlaburl): self
    {
        $this->gitlaburl = $gitlaburl;

        return $this;
    }

    public function getAccesstoken(): ?string
    {
        return $this->accesstoken;
    }

    public function setAccesstoken(?string $accesstoken): self
    {
        $this->accesstoken = $accesstoken;

        return $this;
    }

    public function getUsernamegitlab(): ?string
    {
        return $this->usernamegitlab;
    }

    public function setUsernamegitlab(?string $usernamegitlab): self
    {
        $this->usernamegitlab = $usernamegitlab;

        return $this;
    }

    public function getUsernamegithub(): ?string
    {
        return $this->usernamegithub;
    }

    public function setUsernamegithub(?string $usernamegithub): self
    {
        $this->usernamegithub = $usernamegithub;

        return $this;
    }

    public function getGitlabispub(): ?bool
    {
        return $this->gitlabispub;
    }

    public function setGitlabispub(?bool $gitlabispub): self
    {
        $this->gitlabispub = $gitlabispub;

        return $this;
    }


    public function getGithubjsn(): ?array
    {
        return $this->githubjsn;
    }

    public function setGithubjsn(?array $githubjsn): self
    {
        $this->githubjsn = $githubjsn;

        return $this;
    }

    public function getGitlabjsn(): ?array
    {
        
        return $this->gitlabjsn;
    }

    public function setGitlabjsn(?array $gitlabjsn): self
    {
        $this->gitlabjsn = $gitlabjsn;

        return $this;
    }

    /**
     * Get the value of githubispub
     */
    public function getGithubispub()
    {
        return $this->githubispub;
    }

    /**
     * Set the value of githubispub
     *
     * @return  self
     */
    public function setGithubispub($githubispub)
    {
        $this->githubispub = $githubispub;

        return $this;
    }

    /**
     * Get the value of gitlabaccesstoken
     */ 
    public function getGitlabaccesstoken()
    {
        return $this->gitlabaccesstoken;
    }

    /**
     * Set the value of gitlabaccesstoken
     *
     * @return  self
     */ 
    public function setGitlabaccesstoken($gitlabaccesstoken)
    {
        $this->gitlabaccesstoken = $gitlabaccesstoken;

        return $this;
    }

    /**
     * Get the value of githubaccesstoken
     */ 
    public function getGithubaccesstoken()
    {
        return $this->githubaccesstoken;
    }

    /**
     * Set the value of githubaccesstoken
     *
     * @return  self
     */ 
    public function setGithubaccesstoken($githubaccesstoken)
    {
        $this->githubaccesstoken = $githubaccesstoken;

        return $this;
    }

    public function getGitladid(): ?string
    {
        return $this->gitladid;
    }

    public function setGitladid(string $gitladid): self
    {
        $this->gitladid = $gitladid;

        return $this;
    }

  
 
    public function isOwner(User $user): ?bool
    {
          return $this->gitowner === $user;
    }

 

    public function getGitowner(): ?User
    {
        return $this->gitowner;
    }

    public function setGitowner(?User $gitowner): self
    {
        $this->gitowner = $gitowner;

        return $this;
    }

    
}
