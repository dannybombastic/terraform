<?php

namespace App\Entity;

use App\Repository\SkillRepository;
use App\Service\IOwner;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=SkillRepository::class)
 */
class Skill implements IOwner
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Please Enter atleast one name")
     */
    private $techname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $techdescription;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Positive
     * @Assert\Type(
     *     type="integer",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $techpercent;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="skills")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $techasset;

 
    public function setUserid(?User $userid): self
    {
        $this->userid = $userid;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTechname(): ?string
    {
        return $this->techname;
    }

    public function setTechname(string $techname): self
    {
        $this->techname = $techname;

        return $this;
    }

    public function getTechdescription(): ?string
    {
        return $this->techdescription;
    }

    public function setTechdescription(string $techdescription): self
    {
        $this->techdescription = $techdescription;

        return $this;
    }

    public function getTechpercent(): ?int
    {
        return $this->techpercent;
    }

    public function setTechpercent(int $techpercent): self
    {
        $this->techpercent = $techpercent;

        return $this;
    }

    public function getTechasset(): ?string
    {
        return $this->techasset;
    }

    public function setTechasset(?string $techasset): self
    {
        $this->techasset = $techasset;

        return $this;
    }

    public function isOwner(User $user): ?bool
    {
          return $this->userid === $user;
    }

    /**
     * Get the value of userid
     */ 
    public function getUserid(): User
    {
        return $this->userid;
    }
}
