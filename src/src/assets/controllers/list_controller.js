import { Controller } from "stimulus";

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="list" attribute will cause
 * this controller to be executed. The name "list" comes from the filename:
 * list_controller.js -> "list"
 *
 * Delete this file or adapt it for your use!
 */
export default class extends Controller {
  static targets = ["card"];

  connect() {
    console.log("this es the elements", this.cardTarget, this.bynameTarget);
  }

  byexperience(event) {
    event.currentTarget.dataset.order =
      event.currentTarget.dataset.order == "1"
        ? (event.currentTarget.dataset.order = 0)
        : (event.currentTarget.dataset.order = 1);

    console.log("flag", event.currentTarget.dataset.order);
    let list = $(this.cardTarget.children);

    list.sort(function (a, b) {
      return Number(event.currentTarget.dataset.order) > 0
        ? Number($(a).data("experience")) < Number($(b).data("experience"))
          ? 1
          : -1
        : Number($(a).data("experience")) > Number($(b).data("experience"))
        ? 1
        : -1;
    });
    $(list).appendTo(this.cardTarget);
  }

  byname(event) {
    event.currentTarget.dataset.order =
      event.currentTarget.dataset.order == "1"
        ? (event.currentTarget.dataset.order = 0)
        : (event.currentTarget.dataset.order = 1);

    console.log("flag", event.currentTarget.dataset.order);
    let list = $(this.cardTarget.children);

    list.sort(function (a, b) {
      return Number(event.currentTarget.dataset.order) > 0
        ? $(a).data("name") < $(b).data("name")
          ? 1
          : -1
        : $(a).data("name") > $(b).data("name")
        ? 1
        : -1;
    });
    $(list).appendTo(this.cardTarget);
  }
}
