import { Controller } from "stimulus";

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="list" attribute will cause
 * this controller to be executed. The name "list" comes from the filename:
 * list_controller.js -> "list"
 *
 * Delete this file or adapt it for your use!
 */
export default class extends Controller {
  static targets = ["name", "email"];
  connect() {
    console.log("this es the elements", this.nameTarget.innerHTML);
    console.log("this es the elements", this.emailTarget.innerHTML);
  }
}
