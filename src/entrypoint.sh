#!/usr/bin/env bash
# Use this script to test if a given TCP host/port are available

cd /var/www

composer install

npm install --force

wait-for-it.sh database:5432

bin/console doctrine:database:create

bin/console make:migration -n

bin/console doctrine:migrations:migrate -n

bin/console doctrine:fixtures:load -n

php-fpm 