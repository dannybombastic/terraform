# creamos un dominio
resource "digitalocean_domain" "dannybombastic" {
    name = "dannybombastic.com"
}

# añadimos una etiqueta record al domino
resource "digitalocean_record" "www" {
    domain = digitalocean_domain.dannybombastic.name
    type   = "A"
    name   = "www"
    ttl    = "40"
    value  = digitalocean_droplet.web.ipv4_address
}

resource "digitalocean_record" "flat" {
    domain = digitalocean_domain.dannybombastic.name
    type   = "A"
    name   = "@"
    ttl    = "40"
    value  = digitalocean_droplet.web.ipv4_address
}

