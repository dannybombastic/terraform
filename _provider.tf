terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "2.7.0"
    }
  }
}

variable "DIGITALOCEAN_TOKEN" {}

provider "digitalocean" {
  token = var.DIGITALOCEAN_TOKEN
}