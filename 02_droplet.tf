# creamos instacia en digital ocean se llaman droplet

resource "digitalocean_droplet" "web"  {
  

    image     = "ubuntu-20-04-x64"
    name      = "portfolio-web"
    region    = "nyc1"
    size      = "s-1vcpu-2gb"
    user_data = file("userdata.yml")
    ssh_keys   = [digitalocean_ssh_key.portfolio.fingerprint]

    connection {
        type  = "ssh"
        host  = self.ipv4_address
        user  = "root"
        agent = true
    }

    # provisioner "file" {
    #     source      = "src/"
    #     destination = "/root/"
    # }

  #   provisioner "remote-exec" {

  #   inline = [
  #     # "apt-get update -y",
  #     # "chmod +x /root entrypoint.sh",
  #     # "apt -y install  docker.io",
  #     # "curl -L 'https://github.com/docker/compose/releases/download/1.28.5/docker-compose-$(uname -s)-$(uname -m)' -o /usr/local/bin/docker-compose",
  #     # "chmod +x /usr/local/bin/docker-compose",
  #     # "apt install -y python3-pip",
  #     # "pip3 install --upgrade --force-reinstall --no-cache-dir docker-compose && ln -sf /usr/local/bin/docker-compose /usr/bin/docker-compose",
  #     # "docker network create proxy",
  #     # "cd /root && chown -R  www-data:www-data .",
  #     # "cd /root && docker-compose up -d --build"
  #   ]
  # }



}

output "instance_ip_addr" {

      value = digitalocean_droplet.web.ipv4_address
      description = "The public IP address of the main server instance."
  }

